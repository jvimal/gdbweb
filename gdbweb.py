import gdb
import json
import logging
import multiprocessing
import threading
import flask
import re
import os

logging.basicConfig()
logger = logging.getLogger('root')
logger.setLevel(logging.DEBUG)

pat_array = re.compile(r'(\[\d+\])')

script_path = os.path.dirname(os.path.expanduser(__file__))
app = flask.Flask(__name__,
                  static_folder=os.path.join(script_path, 'static'),
                  static_url_path='/static')

@app.route("/")
def index():
    return flask.redirect("/static/main.html")

@app.route("/sym")
def cmd():
    symbol = flask.request.args.get('name', None)
    if symbol is None:
        resp = flask.make_response(json.dumps({}))
        resp.headers['Content-Type'] = 'application/json'
        return resp
    try:
        print("Processing: %s" % symbol)
        val = gdb.parse_and_eval(symbol)
        val = extract(val)
        status = 0
    except Exception as err:
        status = 1
        err = "Error: %s" % err
        val = {'error': err}
    val['name'] = symbol
    ret = {
        'status': status,
        'data': val
    }
    resp = flask.make_response(json.dumps(ret))
    resp.headers['Content-Type'] = 'application/json'
    return resp

def gdb_code_to_str(code):
    return {
        gdb.TYPE_CODE_PTR:                      "TYPE_CODE_PTR",
        gdb.TYPE_CODE_ARRAY:                    "TYPE_CODE_ARRAY",
        gdb.TYPE_CODE_STRUCT:                   "TYPE_CODE_STRUCT",
        gdb.TYPE_CODE_UNION:                    "TYPE_CODE_UNION",
        gdb.TYPE_CODE_ENUM:                     "TYPE_CODE_ENUM",
        gdb.TYPE_CODE_FLAGS:                    "TYPE_CODE_FLAGS",
        gdb.TYPE_CODE_FUNC:                     "TYPE_CODE_FUNC",
        gdb.TYPE_CODE_INT:                      "TYPE_CODE_INT",
        gdb.TYPE_CODE_FLT:                      "TYPE_CODE_FLT",
        gdb.TYPE_CODE_VOID:                     "TYPE_CODE_VOID",
        gdb.TYPE_CODE_SET:                      "TYPE_CODE_SET",
        gdb.TYPE_CODE_RANGE:                    "TYPE_CODE_RANGE",
        gdb.TYPE_CODE_STRING:                   "TYPE_CODE_STRING",
        gdb.TYPE_CODE_BITSTRING:                "TYPE_CODE_BITSTRING",
        gdb.TYPE_CODE_ERROR:                    "TYPE_CODE_ERROR",
        gdb.TYPE_CODE_METHOD:                   "TYPE_CODE_METHOD",
        gdb.TYPE_CODE_METHODPTR:                "TYPE_CODE_METHODPTR",
        gdb.TYPE_CODE_MEMBERPTR:                "TYPE_CODE_MEMBERPTR",
        gdb.TYPE_CODE_REF:                      "TYPE_CODE_REF",
        gdb.TYPE_CODE_CHAR:                     "TYPE_CODE_CHAR",
        gdb.TYPE_CODE_BOOL:                     "TYPE_CODE_BOOL",
        gdb.TYPE_CODE_COMPLEX:                  "TYPE_CODE_COMPLEX",
        gdb.TYPE_CODE_TYPEDEF:                  "TYPE_CODE_TYPEDEF",
        gdb.TYPE_CODE_NAMESPACE:                "TYPE_CODE_NAMESPACE",
        gdb.TYPE_CODE_DECFLOAT:                 "TYPE_CODE_DECFLOAT",
        gdb.TYPE_CODE_INTERNAL_FUNCTION:        "TYPE_CODE_INTERNAL_FUNCTION",
        }.get(code, "<Unknown>")

def extract_list(head):
    list_members = []
    entry = head['next']
    while entry != head.address:
        list_members.append(entry)
        entry = entry['next']
    return list_members

def extract(val, recurse_depth=1):
    """Returns a dict representation of gdb.Value @val by extracting
    important fields"""
    if recurse_depth < 0:
        return {}
    ret = {}
    ret['address'] = str(val.address)
    ret['type'] = {
        'name': str(val.type.tag),
        'repr': str(val.type),
        'fields': [],
        'code': gdb_code_to_str(val.type.code)
    }
    ret['value'] = ''
    ret['bitpos'] = '0'
    ret['members'] = []
    # print("%s, %s" % (val.type.code, gdb_code_to_str(val.type.code)))
    if val.type.code == gdb.TYPE_CODE_ARRAY and "char" in str(val.type):
        try:
            ret['value'] = "\"%s\"" % flask.escape(val.string())
        except:
            pass
    fields = []
    bitpos = {}
    try:
        for f in val.type.fields():
            fields.append(f.name)
            bitpos[f.name] = f.bitpos
    except:
        # This can happen if the "val" is a primitive type, e.g. char/int/etc.
        return ret
    ret['type']['fields'] = fields

    if len(fields) == 0:
        ret['value'] = "%s" % flask.escape(val)

    members = []
    if val.type.code == gdb.TYPE_CODE_ARRAY and "char" not in str(val.type):
        # Treat it as struct with 0..size-1 members
        try:
            type_name = pat_array.sub('', str(val.type)).strip()
            pointer = False
            if '*' in type_name:
                pointer = True
                type_name = type_name.replace('*','').strip()
            type = gdb.lookup_type(type_name)
            if pointer:
                type = type.pointer()
            type_size = type.sizeof
            array_size = val.type.sizeof
            for i in range(array_size/type_size):
                info = {'name': '[%d]' % i}
                info.update(extract(val[i], recurse_depth-1))
                members.append(info)
            ret['members'] = members
        except Exception as err:
            print('Exception: %s' % err)
            pass
    if recurse_depth == 0:
        return ret

    for f in fields:
        if f is None:
            continue
        info = {'name': f}
        info.update(extract(val[f], recurse_depth-1))
        info.update({'bitpos': bitpos[f]})
        members.append(info)

    ret['members'] = members
    if str(val.type) == 'struct list_head':
        # replace members with elements of this list
        list_entries = extract_list(head=val)
        members = []
        for i, entry in enumerate(list_entries):
            info = {
                'name': '[%d]' % i,
                'address': '%s' % flask.escape(entry.address),
                'value': '%s' % flask.escape(entry),
                'type': {
                    'name': "%s" % flask.escape(str(entry.type.tag)),
                    'repr': "%s" % flask.escape(str(entry.type)),
                    'fields': [],
                    'code': gdb_code_to_str(entry.type.code)
                    },
                'members': [],
                'bitpos': 0,
                }
            members.append(info)
        ret['members'] = members
    return ret

if __name__ == "__main__":
    # gdb.execute("target remote localhost:1234")
    gdb.execute("target remote /sys/kernel/debug/gtp")
    app.run()
