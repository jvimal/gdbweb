var error = function(msg) {
	return $("<div>").prop("class", "error").html(msg);
}

var format_cmd = function(cmd) {
	var node = $("<div>").prop("class", "cmd");
	var t = new Date();
	node.append($("<span>").prop("class", "time").html(t.toTimeString() + ": "));
	node.append($("<span>").html(cmd));
	return node;
}

var fetch_value = function(val, callback) {
	$.getJSON("/sym", {name: val}, callback);
}

var do_command = function(cmd) {
	if(cmd == "") {
		return;
	}
	var history_item = $("<li>").html(format_cmd(cmd));
	var konsole = $("#console");
	var history = $("#console ol#history");
	history_item.addClass("loading");
	history.append(history_item);
	konsole.animate({ scrollTop: konsole.prop("scrollHeight") }, 100);
	fetch_value(cmd, function(data) {
		if(data.status == 0) {
			var value = new Value(data.data);
			var view = new ValueView({model: value});
			history_item.append(view.render().el);
		} else {
			history_item.append(error(data.data));
		}
		konsole.animate({ scrollTop: konsole.prop("scrollHeight") }, 100);
		history_item.removeClass("loading");
	});
}

var do_lookup_and_insert  = function(cmd, model, view) {
	if(view) {
		view.visible = false;
		view.fetched = false;
		$(view.el).addClass("loading");
	}
	fetch_value(cmd, function(data) {
		$(view.el).removeClass("loading").effect("highlight", {}, 2000);
		view.fetched = true;
		view.visible = true;
		if(data.status == 0) {
			data.data.members = new ValueNodes(data.data.members);
			var tempname = model.get("name");
			model.set(data.data);
			model.set({ name: tempname });
		} else {
			model.set(data.data);
		}
	});
}

var do_dereference = function(model, view) {
	var type = model.get("type").repr;
	var addr = model.get("value");
	var val = sprintf("*(%s)(%s)", type, addr);
	do_lookup_and_insert(val, model, view);
}

var Value = Backbone.Model.extend({
	initialize: function() {
		var members = this.get("members");
		if(Array.isArray(members) && members.length > 0) {
			this.set({
				members: new ValueNodes(members)
			});
		}
	},
});

var ValueNodes = Backbone.Collection.extend({
	model: Value,
});

var ValueView = Backbone.View.extend({
	tagName: "div",
	className: "value",

	initialize: function() {
		if(this.model === undefined)
			return;
		this.model.bind("change", this.render, this);
		this.visible = this.model.get("members").length > 0;
		if(this.model.get("type").code == "TYPE_CODE_ARRAY") {
			this.visible = this.model.get("members").length < 5;
		}
		this.fetched = this.model.get("members").length > 0;
	},

	toggle: function() {
		var view = this;
		if(this.fetched == false) {
			var type = this.model.get("type").repr;
			var query = sprintf("* (%s *)(%s)", type, this.model.get("address"));
			do_lookup_and_insert(query, this.model, this);
		}
		if(this.visible == true) {
			this.children.hide();
			this.togg.html("+");
			this.visible = false;
		} else {
			this.children.show();
			this.togg.html("-");
			this.visible = true;
		}
	},

	reload: function() {
		this.fetched = false;
		this.visible = !this.visible;
		this.toggle();
	},

	render: function() {
		var m = this.model;
		var view = this;
		var el = $(this.el).html("");
		var togg = $("<span>").addClass("toggle").html("");
		var code = m.get("type").code;
		if(m.get("type").name != "None" || m.get("type").code == "TYPE_CODE_ARRAY") {
			togg.html(this.visible ? "-" : "+")
				.click($.proxy(this.toggle, this));
		}
		this.togg = togg;
		var type = $("<span>")
			.addClass("type")
			.addClass("col")
			.html(m.get("type").repr);

		if(m.get("type").repr == "struct list_head *") {
			var parent = view.options.parent_model;
			var bitpos = view.options.bitpos;
			var link = $("<a>").addClass("typelink").click(function() {
				var parent_type = '';
				if(!parent) {
					parent_type = prompt("Parent container type missing, enter 'struct type_name' to which you want to typecast: ");
				} else {
					parent_type = parent.get("type").repr;
				}
				if(!bitpos) {
					bitpos = prompt(sprintf("Enter the BIT POSITION of this list in '%s'", parent_type));
				}
				var query = sprintf("*(%s *)(%s-%s/8)",
						parent_type,
						m.get("value"),
						bitpos
					);
				do_lookup_and_insert(query, m, view);
			}).html(m.get("type").repr);
			type.html(link);
		}

		var name = $("<span>")
			.addClass("name")
			.html(m.get("name"));

		if(m.get("error")) {
			var msg = $("<span>").addClass("error").html(m.get("error"));
			var li = $("<li>").append(msg);
			this.visible = true;
			this.fetched = true;
			togg.html(this.visible ? "-" : "+")
				.click($.proxy(this.toggle, this));
			el.append(togg,
					  type,
					  name,
					  $("<ul>").addClass("fields")
							   .append(li));
			return this;
		}

		if(code == "TYPE_CODE_PTR") {
			var link = $("<a>").html(m.get("name")).prop("href", "#").addClass("pointer");
			link.click(function() {
				do_dereference(m, view);
			});
			name.html(link);
		}

		var eq = $("<span>").addClass("eq").html("=");
		var val = $("<span>").addClass("val").addClass("col").html(m.get("value"));
		var addr = $("<span>").addClass("addr").addClass("col").html(m.get("address"));

		/* Toolbar items; should probably use a separate view? */
		var reload = $("<img>").addClass("reloadbutton")
			.prop("src", "/static/reload.png")
			.click($.proxy(this.reload, this));
		var toolbar = $("<span>").addClass("toolbar").append(reload);

		var children = m.get("members");
		var childs = $("<ul>").addClass("fields");
		for(var i = 0; i < children.length; i++) {
			var child = children.at(i);
			var parent = m;
			var bitpos = child.get("bitpos");
			if(parent.get("type").repr == "struct list_head") {
				parent = view.options.parent_model;
				bitpos = view.options.bitpos;
			}
			var node = new ValueView({
				model: child,
				parent_model: parent,
				bitpos: bitpos
			});
			childs.append($("<li>").html(node.render().el));
		}

		if(this.visible == false) {
			childs.hide();
		}

		el.append(togg,
			type,
			name,
			eq,
			val,
			addr,
			toolbar,
			childs)
		this.children = childs;
		return this;
	}
});

var cmd_history = {
	cmds: [],

	current: 0,

	append: function(c) {
		this.cmds.push(c);
		this.current = this.cmds.length;
	},

	prev: function() {
		this.current = Math.max(this.current - 1, 0);
		return this.cmds[this.current];
	},

	next: function() {
		this.current = Math.min(this.current + 1, this.cmds.length);
		return this.current == this.cmds.length ? "" : this.cmds[this.current];
	}
};

$(function() {
	$("#tty").focus();
	$("#tty").keypress(function(e) {
		var code = e.keyCode ? e.keyCode : e.which;
		if(code == 13) {
			var cmd = $(this).val();
			do_command(cmd);
			cmd_history.append(cmd);
			$(this).val("");
		}
	});
	$("#tty").keyup(function(e) {
		var code = e.keyCode ? e.keyCode : e.which;
		if(code == 38) {
			// up arrow
			$(this).val(cmd_history.prev());
		} else if(code == 40) {
			// down arrow
			$(this).val(cmd_history.next());
		}
	})
});
